package ivanovrb.com.beacons.di

import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ivanovrb.com.injector.Component
import ivanovrb.com.injector.ComponentKey
import ivanovrb.com.injector.Initializer
import ivanovrb.com.main.di.MainInnerComponent

@Module
class AppModule {

    @Provides
    @IntoMap
    @ComponentKey(MainInnerComponent::class)
    fun provideMainComponentInitializer(component: Component): Component = component

}