package ivanovrb.com.beacons.di

import com.ivanovrb.uicommon.UiProvider
import com.ivanovrb.uicommon.di.providers.MainExportProvider
import dagger.Component
import ivanovrb.com.beacons.BeaconsApp
import ivanovrb.com.injector.DeviceProvider
import ivanovrb.com.injector.StorageProvides
import javax.inject.Singleton


@Component(
    dependencies = [
        DeviceProvider::class,
        StorageProvides::class,
        MainExportProvider::class
    ],
    modules = [
        AppModule::class
    ]
)
@Singleton
interface AppComponent : UiProvider {
    fun inject(beaconsApp: BeaconsApp)
}
