package ivanovrb.com.beacons

import android.app.Application
import android.content.Context
import android.content.ContextWrapper
import androidx.multidex.MultiDex
import com.ivanovrb.device.di.DeviceComponent
import com.ivanovrb.uicommon.UiApp
import com.ivanovrb.uicommon.UiProvider
import ivanovrb.com.InjectProvider
import ivanovrb.com.beacons.di.AppComponent
import ivanovrb.com.beacons.di.DaggerAppComponent
import ivanovrb.com.injector.Component
import ivanovrb.com.injector.Initializer
import ivanovrb.com.main.di.DaggerMainExportComponent
import ivanovrb.com.storage.di.StorageComponent
import java.lang.IllegalArgumentException
import javax.inject.Inject
import javax.inject.Provider

class BeaconsApp : Application(), UiApp {

    @Inject
    lateinit var componentsMap: @JvmSuppressWildcards Map<Class<out Component>, Provider<Component>>

    @Suppress("unchecked_cast")
    override fun provider(provider: Class<out Component>): InjectProvider<ContextWrapper> {
        return componentsMap[provider]?.get() as? InjectProvider<ContextWrapper>
            ?: throw IllegalArgumentException("Component is not InjectProvider<ContextWrapper>")
    }

    private val appComponent: AppComponent
        get() {
            val repo = StorageComponent.Initializer.init(this)
            val mainExport = DaggerMainExportComponent.builder().app(this).build()
            return DaggerAppComponent.builder()
                .deviceProvider(DeviceComponent.Initializer.init(this))
                .mainExportProvider(mainExport)
                .storageProvides(repo)
                .build()
        }
    override val mainProvider: UiProvider = appComponent


    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}

