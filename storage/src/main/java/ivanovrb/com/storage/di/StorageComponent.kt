package ivanovrb.com.storage.di

import dagger.*
import ivanovrb.com.injector.App
import ivanovrb.com.injector.PreferencesStorage
import ivanovrb.com.injector.StorageProvides
import ivanovrb.com.storage.SharedPreferencesStorage
import javax.inject.Singleton

@Component(modules = [StorageModule::class])
@Singleton
interface StorageComponent : StorageProvides {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun app(app: App): Builder

        fun build(): StorageComponent
    }

    interface Initializer {
        companion object {
            fun init(app: App): StorageComponent = DaggerStorageComponent.builder().app(app).build()
        }
    }
}

@Module
class StorageModule {

    @Provides
    fun providePreferencesStorage(sharedPreferencesStorage: SharedPreferencesStorage): PreferencesStorage =
        sharedPreferencesStorage
}