package ivanovrb.com.test

import com.ivanovrb.device.di.DeviceComponent
import com.ivanovrb.uicommon.UiProvider
import com.ivanovrb.uicommon.di.providers.MainExportProvider
import com.ivanovrb.uicommon.di.providers.StartActionProvider
import dagger.Component
import ivanovrb.com.injector.App
import ivanovrb.com.injector.DeviceProvider
import ivanovrb.com.injector.StorageProvides
import ivanovrb.com.main.di.DaggerMainExportComponent
import ivanovrb.com.storage.di.StorageComponent

@Component(
    modules = [TestModule::class],
    dependencies = [
        DeviceProvider::class,
        StorageProvides::class,
        MainExportProvider::class
    ]
)
interface TestComponent : UiProvider {
    fun inject(testApp: TestApp)
}