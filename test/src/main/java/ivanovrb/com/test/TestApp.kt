package ivanovrb.com.test

import android.app.Application
import com.ivanovrb.uicommon.UiApp
import com.ivanovrb.uicommon.UiProvider

abstract class TestApp : Application(), UiApp {

    abstract val testComponent: TestComponent
    override val mainProvider: UiProvider by lazy { testComponent }

    override fun onCreate() {
        super.onCreate()
        testComponent.inject(this)
    }
}