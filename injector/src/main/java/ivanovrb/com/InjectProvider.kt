package ivanovrb.com

interface InjectProvider<T> {
    fun inject(t: T)
}