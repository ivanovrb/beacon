package ivanovrb.com.injector

import javax.inject.Provider
import kotlin.reflect.KClass

interface App {
    val mainProvider: MainProvider
}