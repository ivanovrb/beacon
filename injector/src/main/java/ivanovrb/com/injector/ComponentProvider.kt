package ivanovrb.com.injector

import javax.inject.Provider

interface TestInjector {
    fun inject(t: Any)
}

//interface ComponentProvider{
//    fun provideInitializer(): Initializer<Any>
//}
