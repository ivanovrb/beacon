package ivanovrb.com.injector

import dagger.MapKey
import kotlin.reflect.KClass

@Target(
    AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@Retention(AnnotationRetention.RUNTIME)
@MapKey
annotation class ComponentKey(val value: KClass<out Component>)
//annotation class ComponentKey(val value: KClass<out Initializer<*>>)