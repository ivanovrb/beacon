package ivanovrb.com.injector.scopes

import dagger.MapKey
import ivanovrb.com.injector.ProviderWrapper
import kotlin.reflect.KClass

@Target(
    AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@Retention(AnnotationRetention.RUNTIME)
@MapKey
annotation class ProviderKey(val value:KClass<out ProviderWrapper>)