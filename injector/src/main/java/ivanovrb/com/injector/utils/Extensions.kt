package ivanovrb.com.injector.utils

import com.ivanovrb.core.utils.ResultException
import io.reactivex.Single

fun<T> Single<T>.mapItToResult():Single<Result<T>>{
    return this.map { Result.Success(it) as Result<T> }
            .onErrorReturn { Result.Failure(ResultException(it)) }
}

fun<T> T?.toResult():Result<T>{
    return if (this != null) Result.Success(this)
    else Result.Failure(Throwable("Null object"))
}