package com.ivanovrb.core.utils


object ErrorCode{
    const val UNKNOWN = 1
}
class ResultException(
        cause: Throwable?,
        val code:Int = -1

) : Exception(cause)