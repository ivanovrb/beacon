package ivanovrb.com.injector.utils

import java.lang.Exception
import java.util.*

sealed class Result<T> {
    class Success<T>(val value: T) : Result<T>()
    class Failure<T>(val exception: Throwable?) : Result<T>()

    fun isSuccess(): Boolean = this is Success
    fun isFailure(): Boolean = this is Failure
}

fun <T> Result<T>.onFailure(action: (exception: Throwable?) -> Unit): Result<T> {
    if (this is Result.Failure) {
        action(this.exception)
    }
    return this
}

fun <T> Result<T>.onSuccess(action: (value: T) -> Unit): Result<T> {
    if (this is Result.Success) action(this.value)
    return this
}

fun <R> runWithCatching(action: () -> R): Result<R> {
    return try {
        Result.Success(action())
    } catch (throwable: Throwable){
        Result.Failure(throwable)
    }
}