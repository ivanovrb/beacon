package ivanovrb.com.injector.interactors

import io.reactivex.Flowable
import ivanovrb.com.models.Device

interface DeviceManager{
    val isBluetoothOn: Boolean

    fun startMonitorDevices(): Flowable<List<Device>>
    fun clearResources()
    fun stopMonitor()
    val isBluetoothAvailable: Boolean
}