package ivanovrb.com.injector

import ivanovrb.com.injector.interactors.DeviceManager

interface DeviceProvider {
    fun provideDeviceManager(): DeviceManager
}