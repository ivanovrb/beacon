package ivanovrb.com.injector

interface Initializer<T: Component>{
    fun init():T
}

//typealias Initializer<T> = () -> T