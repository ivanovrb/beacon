package ivanovrb.com.injector

import javax.inject.Provider
import kotlin.reflect.KClass

interface MainProvider :MapComponentsProvider, NetworkProvider, StorageProvides, DeviceProvider

interface NetworkProvider {
}

interface StorageProvides {
    fun providePreferencesStorage(): PreferencesStorage
}

interface MapComponentsProvider {
//    fun provideMap(): HashMap<KClass<out Initializer<*>>, Provider<Initializer<*>>>
}