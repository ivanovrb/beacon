package ivanovrb.com.injector

interface PreferencesStorage {
    fun clearData()
}