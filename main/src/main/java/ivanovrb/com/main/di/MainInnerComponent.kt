package ivanovrb.com.main.di

import androidx.lifecycle.ViewModel
import com.ivanovrb.uicommon.UiProvider
import com.ivanovrb.uicommon.di.UiCommonModule
import com.ivanovrb.uicommon.di.scopes.ViewModelKey
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ivanovrb.com.InjectProvider
import ivanovrb.com.main.ui.main.MainActivity
import ivanovrb.com.main.ui.main.vm.MainViewModel


@Component(
    dependencies = [
        UiProvider::class
    ],
    modules = [
        UiCommonModule::class,
        MainModule::class
    ]
)
interface MainInnerComponent : ivanovrb.com.injector.Component, InjectProvider<MainActivity>

@Module
class MainModule {

    @Provides
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun provideViewModel(mainViewModel: MainViewModel): ViewModel = mainViewModel

}

