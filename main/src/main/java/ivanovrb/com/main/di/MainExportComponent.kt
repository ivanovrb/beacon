package ivanovrb.com.main.di

import com.ivanovrb.uicommon.UiApp
import com.ivanovrb.uicommon.di.providers.MainExportProvider
import com.ivanovrb.uicommon.di.providers.StartAction
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import ivanovrb.com.injector.Initializer
import ivanovrb.com.main.di.actions.StartActionImpl


@Component(modules = [MainExportModule::class])
interface MainExportComponent : MainExportProvider {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun app(app: UiApp): Builder

        fun build(): MainExportComponent
    }
}

@Module
class MainExportModule {

    @Provides
    fun provideStartAction(startAction: StartActionImpl): StartAction = startAction

    @Provides
    fun provideComponent(app: UiApp): ivanovrb.com.injector.Component = DaggerMainInnerComponent.builder().uiProvider(app.mainProvider).build()

}

//class ComponentInitializer(
//    private val app: UiApp
//) : MainInitializer<MainInnerComponent> {
//    override fun init(): MainInnerComponent = DaggerMainInnerComponent.builder().uiProvider(app.mainProvider).build()
//}