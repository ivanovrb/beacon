package ivanovrb.com.main.di.actions

import android.content.Context
import com.ivanovrb.uicommon.di.providers.StartAction
import com.ivanovrb.uicommon.utils.open
import ivanovrb.com.main.ui.main.MainActivity
import javax.inject.Inject


class StartActionImpl @Inject constructor() : StartAction {
    override fun openScreen(context: Context) {
        context.open(MainActivity::class.java)
    }
}