package ivanovrb.com.main.ui.main.usecase

import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import ivanovrb.com.injector.App
import ivanovrb.com.injector.interactors.DeviceManager
import ivanovrb.com.models.Device
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class BluetoothDeviceUseCase @Inject constructor(
    private val deviceManager: DeviceManager
) {
    val isBluetoothOn: Boolean get() = deviceManager.isBluetoothOn
    val isBluetoothAvailable: Boolean get() = deviceManager.isBluetoothAvailable

    fun startMonitor(): Flowable<List<Device>> {
        return deviceManager.startMonitorDevices()
            .observeOn(AndroidSchedulers.mainThread())
            .skip(500, TimeUnit.MILLISECONDS)
    }

    fun clearResources() {
        deviceManager.clearResources()
    }

    fun stopMonitor() {
        deviceManager.stopMonitor()
    }
}