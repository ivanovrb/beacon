package ivanovrb.com.main.ui.main.adapters

import android.bluetooth.BluetoothClass
import android.bluetooth.BluetoothDevice
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ivanovrb.uicommon.utils.gone
import com.ivanovrb.uicommon.utils.inflate
import com.ivanovrb.uicommon.utils.visible
import ivanovrb.com.main.R
import ivanovrb.com.models.Device
import kotlinx.android.synthetic.main.item_device.view.*

class DeviceAdapter : ListAdapter<Device, DeviceAdapter.DeviceViewHolder>(object : DiffUtil.ItemCallback<Device>() {
    override fun areItemsTheSame(oldItem: Device, newItem: Device): Boolean = oldItem.uuid == newItem.uuid

    override fun areContentsTheSame(oldItem: Device, newItem: Device): Boolean = oldItem === newItem
}) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeviceViewHolder {
        val view = parent.inflate(R.layout.item_device)
        return DeviceViewHolder(view)
    }

    override fun onBindViewHolder(holder: DeviceViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class DeviceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(device: Device) {
            with(itemView) {
                device.uuid?.let { uuid_view.text = it } ?: uuid_view.gone()
                device.name?.let { name_view.text = it } ?: name_view.gone()
                device.rssi?.let { rssi_view.text = String.format("${it}dB") } ?: gone()

                if (device.isBeacon) {
                    beacon_icon.setImageResource(R.drawable.ic_beacron_24_black)
                    device.major?.let {
                        major_view.text = String.format("Major: $it")
                        major_view.visible()
                    }
                    device.minor?.let {
                        minor_view.text = String.format("Minor: $it")
                        minor_view.visible()
                    }
                    device.txPower?.let {
                        tx_power_view.text = String.format("TxPower: $it")
                        tx_power_view.visible()
                    }
                } else {
                    device.type?.let {
                        beacon_icon.setImageResource(
                            when (it) {
                                BluetoothClass.Device.PHONE_SMART -> R.drawable.ic_smartphone_black_24dp
                                BluetoothClass.Device.COMPUTER_LAPTOP -> R.drawable.ic_laptop_black_24dp
                                BluetoothClass.Device.COMPUTER_DESKTOP -> R.drawable.ic_desktop_windows_black_24dp
                                else -> R.drawable.ic_devices_other_black_24dp
                            }
                        )
                    }
                }

            }
        }
    }
}