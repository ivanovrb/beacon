package ivanovrb.com.main.ui.main

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.View
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider.Factory
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.google.android.material.snackbar.Snackbar
import com.ivanovrb.uicommon.utils.gone
import com.ivanovrb.uicommon.utils.inject
import com.ivanovrb.uicommon.utils.viewModelProvider
import com.ivanovrb.uicommon.utils.visible
import ivanovrb.com.main.R
import ivanovrb.com.main.di.MainInnerComponent
import ivanovrb.com.main.ui.main.adapters.DeviceAdapter
import ivanovrb.com.main.ui.main.vm.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    companion object {
        private const val REQUEST_PERMISSIONS_BLE = 34
        private const val REQUEST_ENABLE_BT = 563
    }

    @Inject
    lateinit var viewModelFactory: Factory
    private lateinit var viewModel: MainViewModel
    private lateinit var snackbar: Snackbar


    private val recyclerView: RecyclerView by lazy { findViewById<RecyclerView>(R.id.recycler_view) }

    private val bluetoothBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent?.action
            when (action) {
                BluetoothAdapter.ACTION_STATE_CHANGED -> {
                    val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)

                    when (state) {
                        BluetoothAdapter.STATE_TURNING_ON -> {
                            startMonitor()
                            error_text.gone()
                            snackbar.dismiss()
                        }
                        BluetoothAdapter.STATE_TURNING_OFF -> {
                            recyclerView.gone()
                            viewModel.stopMonitor()
                            showBluetoothEnableError()
                        }
                    }
                }
            }
        }
    }

    private fun showBluetoothEnableError() {
        error_text.text = getString(R.string.turn_on_ble)
        error_text.visible()
        showErrorSnackbar(getString(R.string.turn_on_ble), R.string.turn_on) {
            turnOnBluetooth()
        }
    }

    private val adapter = DeviceAdapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        inject()

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        (recyclerView.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false


        val intentFilter = IntentFilter().apply {
            addAction(BluetoothAdapter.ACTION_STATE_CHANGED)
        }
        registerReceiver(bluetoothBroadcastReceiver, intentFilter)
    }

    override fun onStart() {
        super.onStart()
        if (!checkPermissions())
            requestPermissions()
        else
            if (viewModel.isBluetoothAvailable) {
                if (viewModel.isBluetoothOn)
                    startMonitor()
                else {
                    showBluetoothEnableError()
                }
            } else {
                error_text.text = getString(R.string.ble_unavailable)
            }

    }

    override fun onStop() {
        super.onStop()
        viewModel.stopMonitor()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(bluetoothBroadcastReceiver)
    }

    private fun startMonitor() {
        viewModel.startMonitor()
            .doOnNext {
                if (it.isNotEmpty()) {
                    error_text.gone()
                    adapter.submitList(it)
                }
            }
            .subscribe()
    }

    private fun turnOnBluetooth() {
        val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        startActivityForResult(intent, REQUEST_ENABLE_BT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_ENABLE_BT -> {
                if (resultCode == Activity.RESULT_CANCELED)
                    showBluetoothEnableError()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_PERMISSIONS_BLE -> {
                if (grantResults.getOrNull(0) != PackageManager.PERMISSION_GRANTED
                    || grantResults.getOrNull(1) != PackageManager.PERMISSION_GRANTED
                    || grantResults.getOrNull(2) != PackageManager.PERMISSION_GRANTED
                    || grantResults.getOrNull(3) != PackageManager.PERMISSION_GRANTED
                )
                    showErrorSnackbar(getString(R.string.need_permissions), R.string.allow) {
                        if (shouldShowBleExplanations())
                            requestPermissions()
                        else
                            goToSettings()
                    }
            }
        }
    }

    private fun showErrorSnackbar(string: String, @StringRes actionText: Int, action: (view: View) -> Unit) {
        snackbar = Snackbar.make(findViewById(R.id.layout), string, Snackbar.LENGTH_INDEFINITE)
            .setAction(android.R.string.cancel) { }
            .setAction(actionText, action)
        snackbar.show()
    }

    private fun goToSettings() {
        val intent =
            Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", packageName, null))
        startActivity(intent)
    }

    private fun shouldShowBleExplanations(): Boolean =
        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.BLUETOOTH)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.BLUETOOTH_ADMIN)
                || ActivityCompat.shouldShowRequestPermissionRationale(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
                || ActivityCompat.shouldShowRequestPermissionRationale(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )

    private fun requestPermissions() {
        if (checkPermissions())
            if (shouldShowBleExplanations())
                showErrorSnackbar(getString(R.string.need_permissions), R.string.allow) {
                    if (shouldShowBleExplanations())
                        requestPermissions()
                    else
                        goToSettings()
                }

        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.BLUETOOTH,
                Manifest.permission.BLUETOOTH_ADMIN,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ),
            REQUEST_PERMISSIONS_BLE
        )
    }

    private fun checkPermissions(): Boolean = isPermissionGranted(Manifest.permission.BLUETOOTH)
            && isPermissionGranted(Manifest.permission.BLUETOOTH_ADMIN)
            && isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)
            && isPermissionGranted(Manifest.permission.ACCESS_COARSE_LOCATION)

    private fun isPermissionGranted(permission: String): Boolean =
        ContextCompat.checkSelfPermission(
            this,
            permission
        ) == PackageManager.PERMISSION_GRANTED

    private fun inject() {
        inject(MainInnerComponent::class.java)
        viewModel = viewModelProvider(viewModelFactory)
    }
}
