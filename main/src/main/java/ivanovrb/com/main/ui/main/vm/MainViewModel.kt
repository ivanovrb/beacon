package ivanovrb.com.main.ui.main.vm

import androidx.lifecycle.ViewModel
import io.reactivex.Flowable
import ivanovrb.com.main.ui.main.usecase.BluetoothDeviceUseCase
import ivanovrb.com.models.Device
import javax.inject.Inject

open class MainViewModel @Inject constructor(
    private val bluetoothDeviceUseCase: BluetoothDeviceUseCase
) : ViewModel() {

    open val isBluetoothOn get() = bluetoothDeviceUseCase.isBluetoothOn
    open val isBluetoothAvailable: Boolean get() = bluetoothDeviceUseCase.isBluetoothAvailable

    open fun startMonitor(): Flowable<List<Device>> {
       return bluetoothDeviceUseCase.startMonitor()
    }

    fun clearResources(){
        bluetoothDeviceUseCase.clearResources()
    }

    open fun stopMonitor() {
        bluetoothDeviceUseCase.stopMonitor()
    }
}