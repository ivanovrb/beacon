package ivanovrb.com.main.ui.di

import androidx.lifecycle.ViewModel
import com.ivanovrb.uicommon.UiProvider
import com.ivanovrb.uicommon.di.UiCommonModule
import com.ivanovrb.uicommon.di.scopes.ViewModelKey
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ivanovrb.com.main.di.MainInnerComponent
import ivanovrb.com.main.ui.main.MainActivityTest
import ivanovrb.com.main.ui.main.vm.MainViewModel
import org.mockito.Mockito

@Component(
    dependencies = [
        UiProvider::class
    ],
    modules = [
        UiCommonModule::class,
        TestMainModule::class
    ]
)
interface TestMainComponent : MainInnerComponent {
    fun inject(t: MainActivityTest)
}

@Module
class TestMainModule {

    @Provides
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun viewModelProvide(): ViewModel = Mockito.mock(MainViewModel::class.java)
}
