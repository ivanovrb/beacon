package ivanovrb.com.main.ui

import android.content.ContextWrapper
import ivanovrb.com.InjectProvider
import ivanovrb.com.injector.Component
import ivanovrb.com.injector.DeviceProvider
import ivanovrb.com.injector.StorageProvides
import ivanovrb.com.main.di.MainExportComponent
import ivanovrb.com.main.ui.di.DaggerTestMainComponent
import ivanovrb.com.test.DaggerTestComponent
import ivanovrb.com.test.TestApp
import ivanovrb.com.test.TestComponent
import org.mockito.Mockito

class MainApp : TestApp() {

    override val testComponent: TestComponent
        get() {
            return DaggerTestComponent.builder()
                .deviceProvider(Mockito.mock(DeviceProvider::class.java))
                .storageProvides(Mockito.mock(StorageProvides::class.java))
                .mainExportProvider(Mockito.mock(MainExportComponent::class.java))
                .build()
        }


    override fun provider(provider: Class<out Component>): InjectProvider<ContextWrapper> {
        return DaggerTestMainComponent.builder().uiProvider(this.mainProvider).build() as InjectProvider<ContextWrapper>
    }
}