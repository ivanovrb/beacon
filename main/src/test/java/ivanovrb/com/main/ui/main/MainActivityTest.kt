package ivanovrb.com.main.ui.main

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ApplicationProvider
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Flowable
import ivanovrb.com.main.R
import ivanovrb.com.main.ui.MainApp
import ivanovrb.com.main.ui.di.TestMainComponent
import ivanovrb.com.main.ui.main.adapters.DeviceAdapter
import ivanovrb.com.main.ui.main.vm.MainViewModel
import ivanovrb.com.models.Device
import kotlinx.android.synthetic.main.activity_main.*
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.Shadows
import org.robolectric.android.controller.ActivityController
import org.robolectric.annotation.Config
import org.robolectric.shadows.ShadowActivity
import org.robolectric.shadows.ShadowAlertDialog
import org.robolectric.shadows.ShadowDialog
import javax.inject.Inject
import org.mockito.Mockito.`when` as mockWhen

@RunWith(RobolectricTestRunner::class)
@Config(
    application = MainApp::class,
    sdk = [21],
    manifest = "AndroidManifest.xml"
)
class MainActivityTest {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val activityController: ActivityController<MainActivity> by lazy { Robolectric.buildActivity(MainActivity::class.java) }

    private val activity: MainActivity by lazy {
        activityController.get()
    }

    private val viewModel: MainViewModel by lazy { ViewModelProviders.of(activity,viewModelFactory).get(MainViewModel::class.java) }

    private val shadowActivity: ShadowActivity by lazy { Shadows.shadowOf(activity) }

    private val context = ApplicationProvider.getApplicationContext<MainApp>()

    private val application = context as MainApp
    private val shadowApplication = Shadows.shadowOf(application)

    @Before
    fun setUp() {
        shadowApplication.grantPermissions(
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        )

        (application.provider(TestMainComponent::class.java) as TestMainComponent).inject(this)
    }

    @Test
    fun `turn on bluetooth`() {
        whenever(viewModel.isBluetoothAvailable).thenReturn(true)
        whenever(viewModel.isBluetoothOn).thenReturn(false)

        activityController.create().start().resume()
        assertEquals(context.getString(R.string.turn_on_ble), activity.error_text.text.toString())

        val snackbar = activity.findViewById<CoordinatorLayout>(R.id.layout).findViewById<TextView>(R.id.snackbar_text)
        assertEquals(activity.getString(R.string.turn_on_ble), snackbar.text.toString())

        val turnOnButton = activity.findViewById<CoordinatorLayout>(R.id.layout).findViewById<TextView>(R.id.snackbar_action)
        turnOnButton.performClick()

        val dialogIntent = shadowActivity.nextStartedActivityForResult
        assertEquals(BluetoothAdapter.ACTION_REQUEST_ENABLE, dialogIntent.intent.action)
    }

    @Test
    fun `bluetooth is unavailable`() {
        whenever(viewModel.isBluetoothAvailable).thenReturn(false)
        activityController.create().start().resume()

        assertEquals(context.getString(R.string.ble_unavailable), activity.error_text.text.toString())
    }

    @Test
    fun `fetch data`(){
        whenever(viewModel.isBluetoothAvailable).thenReturn(true)
        whenever(viewModel.isBluetoothOn).thenReturn(true)

        val testDevice = Device("uuid", "name", 33, 12, 1, 2, null, System.currentTimeMillis())
        val dataSource = Flowable.fromArray(listOf(testDevice))
        whenever(viewModel.startMonitor()).thenReturn(dataSource)

        activityController.create().start().resume()

        val adapter = activity.findViewById<RecyclerView>(R.id.recycler_view).adapter as? DeviceAdapter
        assertEquals(1, adapter?.itemCount)
    }
}