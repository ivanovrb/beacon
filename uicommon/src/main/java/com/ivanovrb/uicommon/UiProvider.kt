package com.ivanovrb.uicommon

import com.ivanovrb.uicommon.di.providers.MainExportProvider
import com.ivanovrb.uicommon.di.providers.StartActionProvider
import ivanovrb.com.InjectProvider
import ivanovrb.com.injector.MainProvider

interface UiProvider : MainProvider, MainExportProvider