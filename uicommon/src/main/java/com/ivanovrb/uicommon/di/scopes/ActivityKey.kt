package com.ivanovrb.uicommon.di.scopes

import android.app.Activity
import javax.inject.Scope
import kotlin.reflect.KClass

@Scope
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityKey(val value: KClass<out Activity>)