package com.ivanovrb.uicommon.di.providers

import android.content.Context

interface StartActionProvider {
    fun provideStartAction(): StartAction
}

interface StartAction {
    fun openScreen(context: Context)
}