package com.ivanovrb.uicommon.di.providers

import ivanovrb.com.injector.Component
import ivanovrb.com.injector.Initializer

interface MainExportProvider {
    fun provideStartAction(): StartAction
    fun provideComponentInitializer(): Component
}