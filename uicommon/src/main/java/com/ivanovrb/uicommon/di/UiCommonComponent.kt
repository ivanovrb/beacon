package com.ivanovrb.uicommon.di

import androidx.lifecycle.ViewModelProvider
import com.ivanovrb.uicommon.UiApp
import com.ivanovrb.uicommon.UiProvider
import com.ivanovrb.uicommon.utils.ViewModelFactory
import dagger.Binds
import dagger.Component
import dagger.Module

@Component(
    modules = [
        UiCommonModule::class
    ],
    dependencies = [
        UiProvider::class
    ]
)
interface UiCommonComponent {

    interface Initializer {
        companion object {
            fun init(uiApp: UiApp): UiCommonComponent {
                return DaggerUiCommonComponent.builder()
                    .uiProvider(uiApp.mainProvider)
                    .build()
            }
        }
    }
}

@Module
interface UiCommonModule {

    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}