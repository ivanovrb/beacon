package com.ivanovrb.uicommon.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.util.AttributeSet
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.ivanovrb.uicommon.R

/* Created by ivanov.r on 29.08.2018 */

class FloatingActionButtonCustom(context: Context, attributeSet: AttributeSet) :
    FloatingActionButton(context, attributeSet) {

    private val paint = Paint()
    var text: String? = null
        set(value) {
            field = value
            invalidate()
        }

    var textColor = Color.GRAY
        set(value) {
            paint.color = value
            invalidate()
        }

    init {
        paint.color = Color.GRAY
        paint.typeface = Typeface.DEFAULT_BOLD

        context.theme.obtainStyledAttributes(attributeSet, R.styleable.FloatingActionButtonCustom,0,0).apply {
            try {
                getText(R.styleable.FloatingActionButtonCustom_fabText)?.let { text = it.toString() }
                paint.textSize = getDimension(R.styleable.FloatingActionButtonCustom_fabTextSize, 16f)
                paint.color = getColor(R.styleable.FloatingActionButtonCustom_fabTextColor, Color.GRAY)
            } finally {
                recycle()
            }
        }

    }

    override fun onDrawForeground(canvas: Canvas?) {
        super.onDrawForeground(canvas)
    }

    override fun draw(canvas: Canvas?) {
        super.draw(canvas)
        if (text != null)
            canvas?.drawText(
                text!!,
                (measuredWidth - paint.measureText(text)) / 2,
                (measuredHeight / 2) - ((paint.descent() + paint.ascent()) / 2),
                paint
            )
    }

    fun resetText() {
        text = null
        invalidate()
    }
}