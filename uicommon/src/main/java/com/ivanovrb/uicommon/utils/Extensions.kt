package com.ivanovrb.uicommon.utils

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*
import com.ivanovrb.uicommon.UiApp
import ivanovrb.com.InjectProvider
import ivanovrb.com.injector.Component
import ivanovrb.com.injector.Initializer
import ivanovrb.com.injector.utils.runWithCatching

fun ViewGroup.inflate(resourceId: Int): View {
    return LayoutInflater.from(this.context).inflate(resourceId, this, false)
}

inline fun <reified VM : ViewModel> FragmentActivity.viewModelProvider(
    provider: ViewModelProvider.Factory
) = ViewModelProviders.of(this, provider).get(VM::class.java)

inline fun <reified VM : ViewModel> Fragment.viewModelProvider(
    provider: ViewModelProvider.Factory
) =
    ViewModelProviders.of(this, provider).get(VM::class.java)

inline fun <reified VM : ViewModel> Fragment.activityViewModelProvider(
    provider: ViewModelProvider.Factory
) =
    ViewModelProviders.of(requireActivity(), provider).get(VM::class.java)

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    val et = this
    et.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(et.text.toString())
        }
    })
}

fun View.visible() {
    if (this.visibility == View.GONE)
        this.visibility = View.VISIBLE
}

fun View.gone() {
    if (this.visibility == View.VISIBLE)
        this.visibility = View.GONE
}

fun <T : Activity> Context.open(className: Class<T>) {
    this.startActivity(Intent(this, className))
}


fun ContextWrapper.inject(provider: Class<out Component>){
    (applicationContext as UiApp).provider(provider).inject(this)
}
