package com.ivanovrb.uicommon.utils

import android.graphics.*
import com.squareup.picasso.Transformation

/**
 * Created by User on 26.06.2016.
 */
class ImageRounded : Transformation {

    override fun transform(source: Bitmap): Bitmap {
        val output = Bitmap.createBitmap(source.width, source.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(output)

        val color = Color.RED
        val paint = Paint()
        val rect = Rect(0, 0, source.width, source.height)
        val rectF = RectF(rect)

        paint.isAntiAlias = true
        canvas.drawARGB(0, 0, 0, 0)
        paint.color = color
        canvas.drawOval(rectF, paint)

        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        canvas.drawBitmap(source, rect, rect, paint)

        source.recycle()

        return output
    }

    override fun key(): String {
        return "square()"
    }
}
