package com.ivanovrb.uicommon

import android.content.ContextWrapper
import ivanovrb.com.InjectProvider
import ivanovrb.com.injector.App
import ivanovrb.com.injector.Component
import ivanovrb.com.injector.Initializer

interface UiApp:App{
    override val mainProvider: UiProvider

    fun provider(provider: Class<out Component>): InjectProvider<ContextWrapper>
}