package com.ivanovrb.device.interactors

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.content.pm.PackageManager
import androidx.test.core.app.ApplicationProvider
import com.ivanovrb.device.TestApp
import com.ivanovrb.device.consumers.DeviceBeaconCosumer
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.TestScheduler
import io.reactivex.subscribers.TestSubscriber
import ivanovrb.com.injector.interactors.DeviceManager
import ivanovrb.com.models.Device
import org.altbeacon.beacon.Beacon
import org.altbeacon.beacon.BeaconManager
import org.altbeacon.beacon.RangeNotifier
import org.altbeacon.beacon.Region
import org.altbeacon.beacon.service.scanner.NonBeaconLeScanCallback
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.any
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnit
import org.robolectric.RobolectricTestRunner
import org.robolectric.Shadows
import org.robolectric.annotation.Config
import org.robolectric.shadow.api.Shadow
import org.robolectric.shadows.ShadowBluetoothAdapter
import org.robolectric.shadows.ShadowBluetoothDevice
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


@RunWith(RobolectricTestRunner::class)
@Config(
    sdk = [21],
    application = TestApp::class
)
class DeviceManagerImplTest {

    @get:Rule
    val mockitoRule = MockitoJUnit.rule()!!

    private lateinit var bluetoothAdapter: BluetoothAdapter
    private lateinit var shadowBluetoothAdapter: ShadowBluetoothAdapter

    private val context = ApplicationProvider.getApplicationContext<TestApp>()
    private val shadowPackageManager = Shadows.shadowOf(context.packageManager)

    private val beaconManager = mock(BeaconManager::class.java)
    private val beaconConsumer = mock(DeviceBeaconCosumer::class.java)
    private val deviceManger: DeviceManager = DeviceManagerImpl(beaconConsumer, beaconManager)

    private val testScheduler = TestScheduler()

    @Before
    fun setUp() {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        shadowBluetoothAdapter = Shadows.shadowOf(bluetoothAdapter)

        RxJavaPlugins.initIoScheduler { testScheduler }
        RxJavaPlugins.setInitIoSchedulerHandler { testScheduler }
        RxJavaPlugins.setIoSchedulerHandler { testScheduler }
    }

    @Test
    fun testBleAvailable() {
        assertTrue(deviceManger.isBluetoothAvailable)
    }

    @Test
    fun testBleEnable() {
        shadowBluetoothAdapter.setEnabled(true)
        assertTrue(deviceManger.isBluetoothOn)
    }

    @Test
    fun testBleDisable() {
        shadowBluetoothAdapter.setEnabled(false)
        assertFalse(deviceManger.isBluetoothOn)
    }

    @Test
    fun testDataFromBroadcast() {
        shadowPackageManager.setSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE, false)

        whenever(beaconManager.setNonBeaconLeScanCallback(ArgumentMatchers.any<NonBeaconLeScanCallback>())).thenCallRealMethod()
        whenever(beaconManager.nonBeaconLeScanCallback).thenCallRealMethod()
        whenever(beaconManager.bind(beaconConsumer)).then { beaconConsumer.onBeaconServiceConnectionListener() }

        val mockAddress = "00:01:12:33:44:55"
        val testSubscriber = TestSubscriber<List<Device>>()
        deviceManger.startMonitorDevices().subscribe (testSubscriber)

        val device = ShadowBluetoothDevice.newInstance(mockAddress)
        beaconManager.nonBeaconLeScanCallback?.onNonBeaconLeScan(device, 65, byteArrayOf())

        testSubscriber.assertNoValues()

        testScheduler.advanceTimeBy(1500, TimeUnit.MILLISECONDS)

        testSubscriber.assertValueCount(1)
            .assertValue { mockAddress == it.getOrNull(0)?.uuid }
            .assertValue { 65 == it.getOrNull(0)?.rssi }

        beaconManager.nonBeaconLeScanCallback?.onNonBeaconLeScan(device, -71, byteArrayOf())

        val mockAddress2 = "99:01:12:33:44:55"
        val device2 = ShadowBluetoothDevice.newInstance(mockAddress2)
        beaconManager.nonBeaconLeScanCallback?.onNonBeaconLeScan(device2, -81, byteArrayOf())

        testSubscriber.assertValueCount(1)

        testScheduler.advanceTimeBy(1500, TimeUnit.MILLISECONDS)

        testSubscriber.assertValueCount(2)
            .assertValueAt(1){ 2 == it.size }
            .assertValueAt(1){ mockAddress == it[0].uuid }
            .assertValueAt(1){ mockAddress2 == it[1].uuid }

        deviceManger.stopMonitor()
        testSubscriber.assertComplete()

    }

    @Test
    fun testDataFromLeScanner() {
        shadowPackageManager.setSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE, true)
        shadowBluetoothAdapter.setState(BluetoothAdapter.STATE_ON)

        val captor = ArgumentCaptor.forClass(RangeNotifier::class.java)

        whenever(beaconManager.bind(beaconConsumer)).then { beaconConsumer.onBeaconServiceConnectionListener() }

        val testSubscriber = TestSubscriber<List<Device>>()
        deviceManger.startMonitorDevices().subscribe (testSubscriber)

        verify(beaconManager, times(1)).addRangeNotifier(captor.capture())
        verify(beaconManager, times(1)).startRangingBeaconsInRegion(ArgumentMatchers.any())

        testSubscriber.assertNoValues()

        val id1 = "2f234454-cf6d-4a0f-adf2-f4911ba9ffa6"
        val id2 = "12"
        val id3 = "34"
        val beacon = Beacon.Builder()
            .setId1(id1)
            .setId2(id2)
            .setId3(id3)
            .build()

        captor.value.didRangeBeaconsInRegion(listOf(beacon), Region("1", listOf()))
        testSubscriber.assertNoValues()

        testScheduler.advanceTimeTo(1500, TimeUnit.MILLISECONDS)
        testSubscriber.assertValueCount(1)
            .assertValue { it.isNotEmpty() }
            .assertValue { id1 == it.getOrNull(0)?.uuid }
            .assertValue { id2 == it.getOrNull(0)?.major.toString() }
            .assertValue { id3 == it.getOrNull(0)?.minor.toString() }

        deviceManger.stopMonitor()
        testSubscriber.assertComplete()
    }
}