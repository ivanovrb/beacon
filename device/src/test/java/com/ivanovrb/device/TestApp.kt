package com.ivanovrb.device

import android.app.Application
import android.content.Context

class TestApp : Application(){
    companion object {
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = this
    }
}