package com.ivanovrb.device.interactors

import android.bluetooth.BluetoothAdapter
import android.content.Context
import com.ivanovrb.device.consumers.DeviceBeaconCosumer
import io.reactivex.*
import io.reactivex.internal.operators.flowable.FlowableBuffer
import io.reactivex.internal.operators.flowable.FlowableBufferTimed
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import ivanovrb.com.injector.interactors.DeviceManager
import ivanovrb.com.models.Device
import org.altbeacon.beacon.BeaconManager
import org.altbeacon.beacon.Region
import org.altbeacon.beacon.service.scanner.NonBeaconLeScanCallback
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class DeviceManagerImpl @Inject constructor(
    private val beacomConsumer: DeviceBeaconCosumer,
    private val beaconManager: BeaconManager
) : DeviceManager {

    private val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    override val isBluetoothOn: Boolean get() = bluetoothAdapter?.isEnabled == true
    override val isBluetoothAvailable: Boolean get() = bluetoothAdapter != null

    private val region1 = Region("region1", listOf())

    private lateinit var devicesCallback: (device: Device?) -> Unit

    private lateinit var emitter: Emitter<Device>
    private val flowable =
        Flowable.create<Device>({ emitter ->
            devicesCallback = { device ->
                if (device == null) {
                    emitter.onComplete()
                } else {
                    emitter.onNext(device)
                }
            }
        }, BackpressureStrategy.LATEST)


    override fun startMonitorDevices(): Flowable<List<Device>> {
        beacomConsumer.onBeaconServiceConnectionListener = {
            beaconManager.setNonBeaconLeScanCallback { bluetoothDevice, i, bytes ->
                if (this::devicesCallback.isInitialized)
                    devicesCallback(
                        Device(
                            bluetoothDevice?.address,
                            bluetoothDevice?.name,
                            i,
                            -1,
                            -1,
                            -1,
                            bluetoothDevice?.type
                        )
                    )
            }
            beaconManager.addRangeNotifier { mutableCollection, region ->
                mutableCollection.forEach { beacon ->
                    if (this::devicesCallback.isInitialized)
                        devicesCallback(
                            Device(
                                beacon.id1.toString(),
                                beacon.bluetoothName,
                                beacon.rssi,
                                beacon.txPower,
                                beacon.id2.toInt(),
                                beacon.id3.toInt(),
                                beacon.beaconTypeCode,
                                isBeacon = true
                            )
                        )
                }
            }
            beaconManager.startRangingBeaconsInRegion(region1)
        }

        beaconManager.bind(beacomConsumer)

        return flowable
            .buffer(1500, TimeUnit.MILLISECONDS, Schedulers.io())
            .map {
                it.map { it.uuid to it }.toMap().values.toList()
            }
            .map {
                it.sortedByDescending { it.rssi }
            }
    }


    override fun clearResources() {
    }

    override fun stopMonitor() {
        beaconManager.stopRangingBeaconsInRegion(region1)
        if (this::devicesCallback.isInitialized)
            devicesCallback(null)
        beaconManager.unbind(beacomConsumer)
    }

}