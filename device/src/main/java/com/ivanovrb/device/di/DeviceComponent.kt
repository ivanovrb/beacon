package com.ivanovrb.device.di

import android.content.Context
import com.ivanovrb.device.interactors.DeviceManagerImpl
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import ivanovrb.com.injector.App
import ivanovrb.com.injector.DeviceProvider
import ivanovrb.com.injector.interactors.DeviceManager
import org.altbeacon.beacon.BeaconManager
import org.altbeacon.beacon.BeaconParser
import javax.inject.Singleton


@Component(dependencies = [], modules = [DeviceModule::class])
@Singleton
interface DeviceComponent : DeviceProvider {
    class Initializer {
        companion object {
            fun init(app:App): DeviceComponent {
                return DaggerDeviceComponent.builder()
                    .app(app)
                    .build()
            }
        }
    }

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun app(app: App): Builder

        fun build(): DeviceComponent
    }
}

@Module
class DeviceModule {

    @Provides
    @Singleton
    fun provideBeaconManager(context: Context): BeaconManager {
        return BeaconManager.getInstanceForApplication(context).apply {
            beaconParsers.add(BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"))
        }
    }

    @Provides
    @Singleton
    fun beaconParser():BeaconParser{
        return BeaconParser()
    }
    @Provides
    @Singleton
    fun provideDeviceMandeger(deviceManagerImpl: DeviceManagerImpl): DeviceManager = deviceManagerImpl

    @Provides
    fun provideContext(app: App): Context = app as Context
//    @Provides
//    fun provideBeacomConsumer(deviceBeaconCosumer: DeviceBeaconCosumer): DeviceBeaconCosumer = deviceBeaconCosumer
}