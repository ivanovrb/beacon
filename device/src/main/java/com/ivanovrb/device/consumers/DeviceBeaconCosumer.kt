package com.ivanovrb.device.consumers

import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.util.Log
import org.altbeacon.beacon.BeaconConsumer
import org.altbeacon.beacon.BeaconManager
import org.altbeacon.beacon.MonitorNotifier
import org.altbeacon.beacon.Region
import javax.inject.Inject

open class DeviceBeaconCosumer @Inject constructor(
    private val context: Context
) : BeaconConsumer {

    var onBeaconServiceConnectionListener: () -> Unit = {}

    override fun getApplicationContext(): Context = context

    override fun unbindService(p0: ServiceConnection?) {
        p0?.let { context.unbindService(p0) }
    }

    override fun bindService(p0: Intent?, p1: ServiceConnection?, p2: Int): Boolean {
        return p1?.let { context.bindService(p0, p1, p2) } == true
    }

    override fun onBeaconServiceConnect() {
        onBeaconServiceConnectionListener()
    }
}