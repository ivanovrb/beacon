package com.ivanovrb.device.consumers

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import ivanovrb.com.models.Device
import org.altbeacon.beacon.BeaconParser

class DevicesBroadcastListener(
    private val callback: (device: Device) -> Unit
) : BroadcastReceiver() {

    lateinit var finishCallback: () -> Unit

    companion object {
        private const val LOADING = 635
        private const val FINISH = 23
    }

    private var status: Int = FINISH
    override fun onReceive(context: Context?, intent: Intent?) {
        val action = intent?.action
        when (action) {
            BluetoothAdapter.ACTION_DISCOVERY_STARTED -> {
                status = LOADING
            }
            BluetoothAdapter.ACTION_DISCOVERY_FINISHED -> {
                status = FINISH
                if(this::finishCallback.isInitialized) finishCallback()
            }

            BluetoothDevice.ACTION_FOUND -> {
                val device: BluetoothDevice =
                    intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
                val rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MIN_VALUE)
                val deviceName = device.name
                val deviceHardwareAddress = device.address
                val type = device.bluetoothClass?.deviceClass

                callback(Device(deviceHardwareAddress, deviceName, rssi.toInt(), -1, -1, -1, type))
            }
        }
    }
}