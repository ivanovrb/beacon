package ivanovrb.com.models

data class Device(
    val uuid: String?,
    val name: String?,
    val rssi: Int?,
    val txPower: Int?,
    val major: Int?,
    val minor: Int?,
    val type: Int? = null,
    val expired: Long = System.currentTimeMillis(),
    val isBeacon: Boolean = false
) {
    val isExpired
        get() = expired > System.currentTimeMillis()
}