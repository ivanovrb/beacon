package ivanovrb.com.splash

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.ivanovrb.uicommon.UiProvider
import com.ivanovrb.uicommon.di.providers.StartAction
import com.ivanovrb.uicommon.utils.viewModelProvider
import ivanovrb.com.injector.App
import ivanovrb.com.splash.di.SplashComponent
import ivanovrb.com.splash.vm.SplashViewModel
import javax.inject.Inject

class SplashActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var splashViewModel: SplashViewModel
    @Inject
    lateinit var startActions: dagger.Lazy<StartAction>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
        startActions.get().openScreen(this)
        finish()
    }

    private fun inject() {
        SplashComponent.Initializer.init((application as App).mainProvider as UiProvider).inject(this)
        splashViewModel = viewModelProvider(viewModelFactory)
    }
}
