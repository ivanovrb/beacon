package ivanovrb.com.splash.vm

import androidx.lifecycle.ViewModel
import ivanovrb.com.splash.usecase.SplashUseCase
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    splashUseCase: SplashUseCase
) :ViewModel(){
}