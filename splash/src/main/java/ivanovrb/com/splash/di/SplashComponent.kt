package ivanovrb.com.splash.di

import androidx.lifecycle.ViewModel
import com.ivanovrb.uicommon.UiProvider
import com.ivanovrb.uicommon.di.UiCommonModule
import com.ivanovrb.uicommon.di.scopes.ViewModelKey
import dagger.Binds
import dagger.Component
import dagger.Module
import dagger.multibindings.IntoMap
import ivanovrb.com.splash.SplashActivity
import ivanovrb.com.splash.vm.SplashViewModel

@Component(
    dependencies = [UiProvider::class],
    modules = [SplashModule::class, UiCommonModule::class]
)
interface SplashComponent {
    fun inject(splashActivity: SplashActivity)

    interface Initializer {
        companion object {
            fun init(uiProvider: UiProvider): SplashComponent = DaggerSplashComponent.builder()
                .uiProvider(uiProvider)
                .build()
        }
    }
}

@Module
interface SplashModule {

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    fun provideSplashViewModel(splashViewModel: SplashViewModel): ViewModel
}